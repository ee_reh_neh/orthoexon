#!/bin/bash

# IGR 16/08/10
# File: extract_coverage_featureCounts.sh

# Launches featureCounts for all files, separately by species, generates matrices of results both clean and dirty. 

# $1: Path to mapped reads (gzipped fastq files)
# $2: Output directory name

if [ $# -ne 4 ]; then
    echo "USAGE: extract_coverage_featureCounts_exons.sh InputDir OutputDir SAFDir secondSpecies"
    exit 1
fi

bamdirectory=$1
outdir=$2
SAFDir=$3
secSpecies=$4

# Location of reference file roots:
secSpeciesOrtho="$SAFDir${secSpecies}_ensembl86_orthoexon_"
humanOrtho="${SAFDir}hg38_ensembl86_orthoexon_"

# Create directories for the output
if [ ! -d $outdir/logs ]; then
    mkdir -p $outdir/logs
fi

# Launch featurecounts for either species at the multiple thresholds
# This loop can be commented out when the time comes. 
for i in 0.95 0.96 0.97 0.98 0.99; do 
    for j in FALSE; do    
        for k in 50 100; do
            for l in 10 20 100; do

                # Non-human species:
                echo -e "#! /bin/bash

                echo 'Launching featureCounts for all $secSpecies samples.'
                if [ -s $outdir/${secSpecies}_counts_matrix_${i}.out ]; then
                    echo 'featureCounts has already been run for this species.'
                else
                    # Launch featureCounts
                    # Building the command:
                    # -T: number of threads, set to 8 by me. 
                    # -F: format of the feature file; set to SAF. See the manual for format specification. File is generated automatically by blat_processing_intraspecies.R
                    # -a: annotation file name - here, species-specific ortho exon file.
                    # -s: assign strandedness to data set. Set to 0, first strand, on the basis of library construction. 
                    # -o: sets output file name
                    # -f: count things at the exon level rather than the gene level.

                    longDir=$bamdirectory/ortho_${j}_${i}_${k}bp_${l}x/$secSpecies/mapped

                    for longList in \`ls \$longDir/*bam\`; do
                        sampName=\`basename \$longList\`
                        secSpeciesList=\"\$longDir/\${sampName} \$secSpeciesList\"
                    done

                    echo \" featureCounts -f -T 8 -F SAF -a ${secSpeciesOrtho}${i}_ortho_${j}_pc.txt -s 0 -o $outdir/${secSpecies}_${i}pc_ortho_${j}_${k}bp_${l}x.out \$secSpeciesList \"        
                    featureCounts -f -T 8 -F SAF -a ${secSpeciesOrtho}${i}_ortho_${j}_pc.txt -s 0 -o $outdir/${secSpecies}_${i}pc_ortho_${j}_${k}bp_${l}x.out \$secSpeciesList 
                    # Clean up output for R
                    sed 's:\/::g' $outdir/${secSpecies}_${i}pc_ortho_${j}_${k}bp_${l}x.out | sed \"s/homeusersntuigrorthoexonsimNGShg38v86_${secSpecies}polyesterortho_${j}_${i}_${k}bp_${l}x${secSpecies}mapped//g\" | sed 's/\.fasta\.bam//g' > $outdir/${secSpecies}_${i}pc_ortho_${j}_${k}bp_${l}x_clean.out
                    exitstatus=( \$? )
                    if [ \${exitstatus}  != 0 ]; then
                        echo ERROR: Failed featureCounts with exit code \${exitstatus}
                        exit \${exitstatus}
                    else 
                        echo Successfully ran featureCounts for all secSpecies samples.
                    fi
                fi
                " | qsub -l walltime=0:50:00,ncpus=8 -o $outdir/logs/secSpecies_${i}pc_${j}_${k}_${l}.out -N fcSecSp$i$j$k$l -V -j oe

                # Humans
                echo -e "#! /bin/bash

                echo 'Launching featureCounts for all human samples.'
                if [ -s $outdir/human_counts_matrix_${i}.out ]; then
                    echo 'featureCounts has already been run for this species.'
                else
                    # Launch featureCounts
                    # Building the command:
                    # -T: number of threads, set to 8 by me. 
                    # -F: format of the feature file; set to SAF. See the manual for format specification. File is generated automatically by blat_processing_intraspecies.R
                    # -a: annotation file name - here, species-specific ortho exon file.
                    # -s: assign strandedness to data set. Set to 0, first strand, on the basis of library construction. 
                    # -o: sets output file name
                    # -f: count things at the exon level rather than the gene level.

                    longDir=$bamdirectory/ortho_${j}_${i}_${k}bp_${l}x/hg38/mapped/

                    for longList in \`ls \$longDir/*bam\`; do
                        sampName=\`basename \$longList\`
                        humanList=\"\$longDir/\${sampName} \$humanList\"
                    done

                    echo \" featureCounts -f -T 8 -F SAF -a ${humanOrtho}${i}_ortho_${j}_pc.txt -s 0 -o $outdir/human_${i}pc_ortho_${j}_${k}bp_${l}x.out \$humanList \"        
                    featureCounts -f -T 8 -F SAF -a ${humanOrtho}${i}_ortho_${j}_pc.txt -s 0 -o $outdir/human_${i}pc_ortho_${j}_${k}bp_${l}x.out \$humanList 
                    # Clean up output for R
                    sed 's:\/::g' $outdir/human_${i}pc_ortho_${j}_${k}bp_${l}x.out | sed \"s/homeusersntuigrorthoexonsimNGShg38v86_${secSpecies}polyesterortho_${j}_${i}_${k}bp_${l}xhg38mapped//g\" | sed 's/\.fasta\.bam//g' > $outdir/human_${i}pc_ortho_${j}_${k}bp_${l}x_clean.out
                    exitstatus=( \$? )
                    if [ \${exitstatus}  != 0 ]; then
                        echo ERROR: Failed featureCounts with exit code \${exitstatus}
                        exit \${exitstatus}
                    else 
                        echo Successfully ran featureCounts for all human samples.
                    fi
                fi
                " | qsub -l walltime=0:50:00,ncpus=8 -o $outdir/logs/human_${i}pc_${j}_${k}_${l}.out -N fcHuman$i$j$k$l -V -j oe
            done
        done
    done
done
