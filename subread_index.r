## ==========================================================================
## File: subread_build_index
##
## Generate base or color-space genome index.
## Must have more than 4GB RAM.
## 
## O.Korn
## 2014-07-15
## ==========================================================================

### Modified by IGR to work with NSCC pipelines etc. 
### 2017.02.05
###
### Command-line executable
### Requires four command line arguments:
### 1. out dir
### 2. out file name
### 3. in fasta file - needs to be an ABSOLUTE PATH. 
### 4. Type of index wanted: "base" or "color" Defaults to base

### command line example: 
### Rscript --vanilla ~/repos/orthoexon/subread_index.r ~/orthoexon/panTro5/ panTro5 /home/users/ntu/igr/orthoexon/panTro5/panTro5.fa base

library("Rsubread") # 1.20.6
library("limma") # 3.26.9
library("tools")

extraVars <- commandArgs(trailingOnly=T)

indexdir <- extraVars[1]
indexname <- extraVars[2]
genomefasta <- extraVars[3]
indexspace <- extraVars[4]

## Index file <indexname>.00.b.tab for base space,
## or <indexname>.00.c.tab for colour space.
fileext <- "00.b.tab"
if (! is.na(indexspace)) {
  indexspace <- as.character(indexspace)
  if (indexspace == "color") {
    fileext <- "00.c.tab"
    colorcheck <- TRUE
  } else {colorcheck <- FALSE}
} else {colorcheck <- FALSE}

usemem <- 0

## Work out how much free memory we have (if running in Unix/Linux)
## If we have 8GB we should fit entire human genome index in memory.
## Subread (at time of writing) uses 3.7GB if not supplied.
if (.Platform$OS.type == "unix") {
  if (file.exists("/proc/meminfo")) {
    memfreeKB <- system("grep -i -P ^memfree /proc/meminfo | sed -r 's|^[^0-9]+([0-9]+).*$|\\1|'", intern=T)
    availKB <- system("grep -i -P ^cached /proc/meminfo | sed -r 's|^[^0-9]+([0-9]+).*$|\\1|'", intern=T)
    if (length(grep("^[0-9]+$", memfreeKB, perl=T)) & length(grep("^[0-9]+$", availKB, perl=T))) {
      totalAvailKB <- as.numeric(memfreeKB) + as.numeric(availKB)
      memfreeMB <- totalAvailKB / 1024
      if (memfreeMB < 4000) {
        stop("Error: Less than 4GB free memory, will not generate genome index")
      }
      usemem <- floor(memfreeMB)
      ## Usable memory = 90% of available - CONSERVATIVE THRESHOLD NOT USED
      #usemem <- floor(memfreeMB * 0.9)
    }
  }
} else if (.Platform$OS.type == "windows") {
  write("Notice: This is a Windows system, will use default memory amount to build index", stderr())
}


## Build index if not already there
idxfile <- paste(indexdir,"/",indexname,".",fileext,sep="")
if (! file.exists(idxfile)) {
   
   setwd(indexdir)

   if (usemem > 0) {
     buildindex(basename=indexname,reference=genomefasta,memory=usemem, colorspace=colorcheck)
   } else {
     buildindex(basename=indexname,reference=genomefasta, colorspace=colorcheck)
   }

} else {
  print(paste("Genome index '",idxfile,"' exists, skipping build"))
}
