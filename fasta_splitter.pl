#!/usr/bin/perl
use warnings;
use File::Basename;
#Split FASTA files into chunks determined by user.
#by RNAeye
# Note IGR 16.06.14: found online at http://seqanswers.com/forums/archive/index.php/t-41122.html
# Usage: perl fasta_splitter.pl [name of FASTA to split] [records per file]
# Command line argument modification by IGR

my $file = $ARGV[0]; #enter name of your FASTA file here
my $record_per_file = $ARGV[1];    #Enter how many record you want per file / chunk size
scalar @ARGV >= 2 or die<<USAGE;
Usage:
perl -w fasta_splitter <FASTA file> <records per split file>
USAGE

my $file_number = 1;    #this is going to be a part of your new file names.
my $counter = 0;    #counts number of records

open (FASTA, "<", "$file" ) or die "Cannot open file $file $!";

while (<FASTA>) {
    if (/^>/) { 
        if ($counter++ % $record_per_file == 0) {
            my $basename = basename($file);
            my $new_file_name = $basename. $file_number++ . ".fa"; 
            close(NEW_FASTA);
            open(NEW_FASTA, ">", $new_file_name) or die "Cannot open file $new_file_name $!";
        }
    }
print NEW_FASTA $_; 
}
