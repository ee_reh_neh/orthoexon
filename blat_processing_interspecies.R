### 16.07.20
### Irene Gallego Romero
### 
### Script for processing output from BLAT for the purposes of generating a pairwise file of high quality orthologous exons. 
### Thresholds for now are modelled on those adopted by Ran in his 2010 Genome Research paper and subsequent 2012 Nature Preceedings manuscript. 

### Last edit: IGR, 2017.01.26
### Fleshed out the indel size filtration step, which had not been properly implemented (not a big difference in results, but still worthwhile)


#########################################
### 0. Prep workspace, load packages. ###
#########################################

rm(list=ls())
options(width=200)
library(data.table)
library(stringi)

#######################################
### 1. Data reading and processing. ### 
#######################################

setwd("~/orthoexon/exon_hg19v89/blat_output/hg19v89_panTro3")
blatOut <- fread("panTro3_blat_all_hg19_exons.out", sep="\t")
dim(blatOut)
#[1] 4756991      21

names(blatOut) <- c("match", "mismatch", "rep.match", "Ns", "Q.gap.count", "Q.gap.bases", "T.gap.count", "T.gap.bases", "strand", "Q.name", "Q.size", "Q.start", "Q.end", "T.name", "T.size", "T.start", "T.end", "block.count", "block.sizes", "qStarts", "tStarts")

# Filtering, as per Ran's thresholds:
    # No indel bigger than 25 bp
    # Different thresholds of %identity; the best of which needs to be experimentally determined.
    # Note that this step is totally agnostic to there being multiple hits
blatOut$pc.ident <- blatOut$match / blatOut$Q.size

blatOutIdent <- blatOut[blatOut$pc.ident >= .90,]
dim(blatOutIdent)
# [1] 953348     22

# Indel filtering: a bit trickier than it seemed before, so we use a function to make things clean.
filterIndels <- function(blatResults, threshold){
    blatResultsSmall <- blatResults[(blatResults$Q.gap.bases <= threshold & blatResults$T.gap.bases <= threshold),] #Small or no indels, set these aside
    print(paste0(dim(blatResultsSmall)[1], " exons have no possible indels > ", threshold))

    blatResultsComplex <- blatResults[(blatResults$Q.gap.bases > threshold | blatResults$T.gap.bases > threshold),] # Everything else 
    print(paste0("Processing the remaining ", dim(blatResultsComplex)[1], " exons"))

    blatResultsComplex <- blatResultsComplex[!(blatResultsComplex$T.gap.bases/blatResultsComplex$T.gap.count > threshold | blatResultsComplex$Q.gap.bases/blatResultsComplex$Q.gap.count > threshold),]    # Drop rows that mathematically must have at least one indel > threshold
    print(paste0("Only ", dim(blatResultsComplex)[1], " exons do not have a mathematically obligate indel >", threshold))

    # Calculating actual gap sizes:
        blatBlocks <- as.numeric(stri_split_fixed(blatResultsComplex$block.size, ",", simplify = TRUE))
            blatBlocks <- `dim<-`(as.numeric(blatBlocks), dim(blatBlocks))  # convert to numeric and save dims

        blatQStart <- stri_split_fixed(blatResultsComplex$qStarts, ",", simplify = TRUE)
            blatQStart <- `dim<-`(as.numeric(blatQStart), dim(blatQStart))  # convert to numeric and save dims

        blatTStart <- stri_split_fixed(blatResultsComplex$tStarts, ",", simplify = T)
            blatTStart <- `dim<-`(as.numeric(blatTStart), dim(blatTStart))  # convert to numeric and save dims

        blatQEnd <- blatBlocks + blatQStart
        blatQEnd <- cbind(0, blatQEnd)
        blatQStart <- cbind(blatQStart,0)

        blatTEnd <- blatBlocks + blatTStart
        blatTEnd <- cbind(0, blatTEnd)
        blatTStart <- cbind(blatTStart,0)

    blatResultsComplex$QFail <- apply((blatQStart - blatQEnd), 1, function(x) any(x > threshold))
    blatResultsComplex$TFail <- apply((blatTStart - blatTEnd)[,-1], 1, function(x) any(x > threshold)) # Drop first column because that's just the transcription start site

    blatResultsFilt <- blatResultsComplex[is.na(blatResultsComplex$QFail) & is.na(blatResultsComplex$TFail),]
    print(paste0(dim(blatResultsFilt)[1], " complicated exons did not have an indel > ", threshold))
    print(paste0("Total number of retained exons after this step is: ", dim(blatResultsFilt)[1] + dim(blatResultsSmall)[1]))

    return(rbind(blatResultsSmall, blatResultsFilt[,1:22]))
}

#Actually filtering:
blatOutIndelIdent <- filterIndels(blatOutIdent, 25)
blatOutIndelIdent <- droplevels(blatOutIndelIdent)
dim(blatOutIndelIdent)
# [1] 885708     22

# Retain the best match for multi-matching exons
# Now using data.table since plyr was being criminally slow about this and also hoarding memory!
# Currently takes about 20 mins
setkey(blatOutIndelIdent, Q.name)
blatFilt <- blatOutIndelIdent[,.SD[which.max(pc.ident)], by="Q.name"]
dim(blatFilt)
#[1] 588689     22

# Write bed file for retrieving species-specific sequence from UCSC or similar:
blatBed <- blatFilt[,c(14,16,17,1,10,22), with=F]  #columns 10 and 22 contain the strand and the percent human/macaque identity, for filtering purposes downstream. 
write.table(blatBed, file="panTro_0.90_orthoexon_filter.bed", sep="\t", eol="\n", col.names=F, row.names=F, quote=F)

# Final exons at multiple identity thresholds 
for (i in seq(0.91, 0.99, 0.01)){
    print(dim(blatFilt[blatFilt$pc.ident >= i,]))
}
