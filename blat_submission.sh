#!/bin/bash

# I gave up on trying to run this as a qsub script with PBS directives; I think the echoing doesn't work well. So that's that. Instead I will submit a lot of jobs.  

# Interspecies first stage:
for i in {1..74}; do
    # Submits blat jobs for everyone. This is far from optimised right now in terms of structure, since the next script requires holding and cat-ing etc, but I just want this done.
    #echo "blat -noHead ~/orthoexon/macFas5/macFas5.fa ~/orthoexon/exon_hg38/hg38-5_all_exons_ensembl_84_160531.fa${i}.fa ~/orthoexon/exon_hg38/blat_output/hg38_to_macFas5/macFas5_${i}_blat.out" | qsub -l mem=4gb,walltime=24:00:00 -V -o ~/logs/blat_hg_macFas_$i.log -N blat_hg_macFas_$i -M igr@ntu.edu.sg -m ae -oe

    #blat -noHead ~/orthoexon/panTro3/panTro3.fa ~/orthoexon/exon_hg38/hg38-5_all_exons_ensembl_84_160531.fa${i}.fa ~/orthoexon/exon_hg38/blat_output/hg38_to_panTro3/panTro3_${i}_blat.out

done

# Back to human second stage:
for i in {1..57}; do
    # Submits blat jobs for everyone. This is far from optimised right now in terms of structure, since the next script requires holding and cat-ing etc, but I just want this done.
    echo "blat -noHead ~/orthoexon/hg38/hg38_renamed_no_alt_analysis_set.fa /home/users/ntu/igr/orthoexon/exon_hg38/blat_output/macFas5_to_macFas5/macFas5_orthoexons_0.90_filter.fa${i}.fa ~/orthoexon/exon_hg38/blat_output/macFas5_to_hg38/hg38_${i}_blat.out" | qsub -l mem=4gb,walltime=24:00:00 -V -o ~/logs/blat_macFas_hg_$i.log -N blat_macFas_hg_$i -M igr@ntu.edu.sg -m ae -oe

    #blat -noHead ~/orthoexon/panTro3/panTro3.fa ~/orthoexon/exon_hg38/hg38-5_all_exons_ensembl_84_160531.fa${i}.fa ~/orthoexon/exon_hg38/blat_output/hg38_to_panTro3/panTro3_${i}_blat.out

done


#Intraspecies third stage:
for i in {1..57}; do

    # Submits blat jobs for everyone. This is far from optimised right now in terms of structure, since the next script requires holding and cat-ing etc, but I just want this done.
    echo "blat -noHead ~/orthoexon/macFas5/macFas5.fa /home/users/ntu/igr/orthoexon/exon_hg38/blat_output/macFas5_to_macFas5/macFas5_orthoexons_0.90_filter.fa${i}.fa ~/orthoexon/exon_hg38/blat_output/macFas5_to_macFas5/macFas5_${i}_blat.out" | qsub -l mem=4gb,walltime=24:00:00 -V -o ~/logs/blat_macFas_autos_$i.log -N blat_macFas_auto_$i -M igr@ntu.edu.sg -m ae -oe

    #echo "blat -noHead ~/orthoexon/hg38/hg38_renamed_no_alt_analysis_set.fa ~/orthoexon/exon_hg38/hg38-5_all_exons_ensembl_84_160531.fa${i}.fa ~/orthoexon/exon_hg38/blat_output/hg38_to_hg38/hg38_${i}_blat.out" | qsub -l mem=8gb,walltime=24:00:00 -V -o ~/logs/blat_hg_hg_$i.log -N blat_hg_hg_$i -M igr@ntu.edu.sg -m ae -oe

done


