#!/bin/bash

### IGR 2017/02/09
### File: run_subread.sh
### Wrapper for subread_align.r, launches one array job for each set of simulated RNA seqs (generated with polyester)


for i in ortho_FALSE; do
    for j in 0.99 0.98 0.97 0.96 0.95; do
        for k in 50 100; do
            for l in hg38 panTro5; do
                for m in 100; do # 20 10; do
                    echo -e "#! /bin/bash

                    if [[ \${PBS_ARRAY_INDEX} != 10 ]]; then
                        BASE=sample_0\${PBS_ARRAY_INDEX}.fasta;
                    elif [[ \${PBS_ARRAY_INDEX} == 10 ]]; then
                        BASE=sample_\${PBS_ARRAY_INDEX}.fasta;
                    fi

                    INPUTDIR=/home/users/ntu/igr/orthoexon/simNGS/hg38v86_panTro5/polyester/${i}_${j}_${k}bp_${m}x/${l};
                    INDEXDIR=~/orthoexon/genome_indexes/;
                    SPECIES=$l;
                    OUTDIR=/home/users/ntu/igr/orthoexon/simNGS/hg38v86_panTro5/polyester/${i}_${j}_${k}bp_${m}x/${l}/mapped/;
                    COLORCONVERT=F;
                    THREADS=4;

                    if [[ $k == 50 ]]; then 
                        MISMATCH=2;
                        INDEL=2;
                    elif [[ $k == 100 ]]; then
                        MISMATCH=4;
                        INDEL=4;
                    fi

                    mkdir -p \$OUTDIR

                    if [ ! -s \$INPUTDIR/\$BASE.gz ]
                    then
                      echo "File is missing or empty: \$INPUTDIR/\$BASE.gz"
                      exit 65
                    fi

                    if [ -s \$OUTDIR/\$BASE.bam ]
                    then
                          echo "Output file already exists: \$OUTDIR/\${BASE}.bam"
                      exit 64
                    fi

                    # Subread does not accept gzipped input, so we gotta unzip:
                    gunzip \$INPUTDIR/\$BASE.gz

                    # Then R
                    module load R
                    Rscript --vanilla ~/repos/orthoexon/subread_align.r \$INPUTDIR \$BASE \$INDEXDIR \$SPECIES \$OUTDIR \$COLORCONVERT \$THREADS \$MISMATCH \$INDEL

		    if [ $?  == 0 ]; then
                        echo "Successfully mapped and rearchived \$INPUTDIR/\$BASE." 
                        gzip \$INPUTDIR/\$BASE
                    else 
                        echo "Failed to map \$INPUTDIT/\$BASE with exit code $?"
                    fi
                    
                    " | qsub -q normal -l walltime=1:00:00,ncpus=4 -o ~/logs/map_poly_${i}_${j}_${k}_${l}_${m}.out -N poly_${i}_${j}_${k}_${l}_${m} -M igr@ntu.edu.sg -m ae -V -j oe -J 1-10
                done
            done
        done
    done
done   
